package edu.eci.cosw.samples;


import edu.eci.cosw.jpa.sample.model.Consulta;
import edu.eci.cosw.jpa.sample.model.Paciente;
import edu.eci.cosw.jpa.sample.model.PacienteId;
import edu.eci.cosw.samples.repository.PatientsRepository;
import edu.eci.cosw.samples.services.PatientServices;
import edu.eci.cosw.samples.services.ServicesException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpringDataRestApiApplication.class)
@WebAppConfiguration
@ActiveProfiles("test")
public class SpringDataRestApiApplicationTests {
    @Autowired
    PatientServices ps;
    @Autowired
    PatientsRepository pr;
        
    @Test
    public void deberiaCargarPacienteExistente(){
        Paciente p= new Paciente(new PacienteId(1,"cc"),"a",new Date());
        pr.saveAndFlush(p);
        try {
            Paciente p2=ps.getPatient(1,"cc");
            Assert.assertEquals("falla consultando paciente existente",p.getNombre(),p2.getNombre());
        } catch (ServicesException e) {
            Assert.fail("arroja excepcion consultando paciente existente");
        }
    }

    @Test
    public void noDeberiaCargarPacienteInexistente(){
        try {
            Paciente p2=ps.getPatient(2,"cc");
            Assert.fail("no arroja excepcion al consultar paciente inexistente");
        } catch (ServicesException e) {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void noDeberiaCargarTopPacientesSiNoHay(){
        try {
            Paciente p= new Paciente(new PacienteId(1,"cc"),"a",new Date());
            pr.saveAndFlush(p);
            List<Paciente> pacientes= ps.topPatients(3);
            Assert.assertEquals("Consulta top pacientes cuando no los hay",pacientes.size(),0);
        } catch (ServicesException e) {
            Assert.fail("arroja excepcion consultando topPacientes cuando no los hay");
        }
    }

    @Test
    public void DeberiaCargarTopPacientes(){
        try {
            Paciente p= new Paciente(new PacienteId(1,"cc"),"a",new Date());
            Paciente p2= new Paciente(new PacienteId(2,"cc"),"b",new Date());
            Set<Consulta> consA= new HashSet<>();
            Consulta ca= new Consulta(new Date(),"Julian Devia");
            consA.add(ca);
            p2.setConsultas(consA);
            Paciente p3= new Paciente(new PacienteId(3,"cc"),"c",new Date());
            Set<Consulta> consB= new HashSet<>();
            Consulta cb= new Consulta(new Date(),"Julian Devia a");
            Consulta cc= new Consulta(new Date(),"Julian Devia b");
            consB.add(cb);
            consB.add(cc);
            p3.setConsultas(consB);
            pr.saveAndFlush(p);
            pr.saveAndFlush(p2);
            pr.saveAndFlush(p3);
            List<Paciente> pacientes= ps.topPatients(1);
            Assert.assertEquals("Falla consultando top pacientes",pacientes.size(),2);
        } catch (ServicesException e) {
            Assert.fail("arroja excepcion consultando topPacientes");
        }
    }

}
