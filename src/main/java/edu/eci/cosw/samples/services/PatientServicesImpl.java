package edu.eci.cosw.samples.services;

import edu.eci.cosw.jpa.sample.model.Paciente;
import edu.eci.cosw.jpa.sample.model.PacienteId;
import edu.eci.cosw.samples.repository.PatientsRepository;
import edu.eci.cosw.samples.services.PatientServices;
import edu.eci.cosw.samples.services.ServicesException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by amoto on 2/26/17.
 */
@Service
public class PatientServicesImpl implements PatientServices {

    @Autowired
    private PatientsRepository pr;

    @Override
    public Paciente getPatient(int id, String tipoid) throws ServicesException {
        Paciente p=pr.findOne(new PacienteId(id,tipoid));
        if(p==null){
            throw new ServicesException("No existe paciente con el id dado");
        }
        return p;
    }

    @Override
    public List<Paciente> topPatients(int n) throws ServicesException {
        return pr.topPatients(n);
    }
}
