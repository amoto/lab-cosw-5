package edu.eci.cosw.samples.repository;

import edu.eci.cosw.jpa.sample.model.Paciente;
import edu.eci.cosw.jpa.sample.model.PacienteId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Julian David Devia Serna on 2/26/17.
 */
public interface PatientsRepository extends JpaRepository<Paciente, PacienteId> {

    @Query("select P from Paciente P where P.consultas.size = :n or P.consultas.size > :n")
    public List<Paciente> topPatients(@Param("n")int n);
}
